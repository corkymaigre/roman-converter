﻿using RomanConverter.Model;
using System;
using System.Windows;
using System.Windows.Input;

namespace RomanConverter.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        Roman roman = null;
        Arabic arabic = null;

        RelayCommand _convertCommand;
        RelayCommand _changeCommand;

        public MainWindowViewModel()
        {

        }

        private string _firstLabel = "Arabic";
        public string FirstLabel
        {
            get { return _firstLabel; }
            set
            {
                if (value != _firstLabel)
                {
                    _firstLabel = value;
                    NotifyPropertyChanged("FirstLabel");
                }
            }
        }

        private string _firstValue;
        public string FirstValue
        {
            get { return _firstValue; }
            set
            {
                if (value != _firstValue)
                {
                    _firstValue = value;
                    NotifyPropertyChanged("FirstValue");
                }
            }
        }

        private string _secondLabel = "Roman";
        public string SecondLabel
        {
            get { return _secondLabel; }
            set
            {
                if (value != _secondLabel)
                {
                    _secondLabel = value;
                    NotifyPropertyChanged("SecondLabel");
                }
            }
        }

        private string _secondValue;
        public string SecondValue
        {
            get { return _secondValue; }
            set
            {
                if (value != _secondValue)
                {
                    _secondValue = value;
                    NotifyPropertyChanged("SecondValue");
                }
            }
        }

        public ICommand ConvertCommand
        {
            get
            {
                if (_convertCommand == null)
                {
                    _convertCommand = new RelayCommand(param => this.ConvertCommandExecute(), param => this.ConvertCommandCanExecute);
                }
                return _convertCommand;
            }
        }

        void ConvertCommandExecute()
        {
            if (FirstValue.Trim() == null)
                MessageBox.Show("You must enter a value.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                if (FirstLabel == "Arabic")
                {
                    int number = 0;
                    try
                    {
                        number = int.Parse(FirstValue.Trim());
                        if (number < 1)
                            MessageBox.Show("You can not enter a negative value", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        else
                        {
                            arabic = new Arabic(FirstValue);
                            SecondValue = arabic.ConvertToRoman(number);
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("You did not enter an arabic number, try again.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }                    
                }
                else if (FirstLabel == "Roman")
                {
                    String number = FirstValue.Trim();
                    roman = new Roman(FirstValue);
                    SecondValue = roman.ConvertToArabic();
                }
            }
        }

        bool ConvertCommandCanExecute
        {
            get { return true; }
        }

        public ICommand ChangeCommand
        {
            get
            {
                if (_changeCommand == null)
                {
                    _changeCommand = new RelayCommand(param => this.ChangeCommandExecute(), param => this.ChangeCommandCanExecute);
                }
                return _changeCommand;
            }
        }

        void ChangeCommandExecute()
        {
            var tmp1 = FirstLabel;
            var tmp2 = SecondLabel;
            FirstLabel = tmp2;
            SecondLabel = tmp1;
        }

        bool ChangeCommandCanExecute
        {
            get { return true; }
        }

    }
}

