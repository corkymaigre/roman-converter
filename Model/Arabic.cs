﻿using System;
using System.Text;

namespace RomanConverter.Model
{
    class Arabic
    {
        private int _arabicValue;
        public int ArabicValue { get; set; }
        private static int max = 3998;

        public Arabic(string str)
        {
        }

        public string ConvertToRoman(int value)
        {
            if (value > max)
                throw new ArgumentOutOfRangeException("value");

            string[,] romanDigits = new string[,] {
                 {"M",      "C",    "X",    "I"    },
                 {"MM",     "CC",   "XX",   "II"   },
                 {"MMM",    "CCC",  "XXX",  "III"  },
                 {null,     "CD",   "XL",   "IV"   },
                 {null,     "D",    "L",    "V"    },
                 {null,     "DC",   "LX",   "VI"   },
                 {null,     "DCC",  "LXX",  "VII"  },
                 {null,     "DCCC", "LXXX", "VIII" },
                 {null,     "CM",   "XC",   "IX"   }
            };

            StringBuilder result = new StringBuilder(15);

            for (int index = 0; index < 4; index++)
            {
                int power = (int)Math.Pow(10, 3 - index);
                int digit = value / power;
                value -= digit * power;
                if (digit > 0)
                    result.Append(romanDigits[digit - 1, index]);
            }

            return result.ToString();
        }

    }
}


