﻿using System.Collections.Generic;
using System.Linq;

namespace RomanConverter.Model
{
    class Roman
    {
        private string _romanValue;
        public string RomanValue { get; set; }

        Dictionary<char, short> numbers = new Dictionary<char, short>();

        public Roman(string str)
        {
            RomanValue = str;
            numbers.Add('M', 1000);
            numbers.Add('D', 500);
            numbers.Add('C', 100);
            numbers.Add('L', 50);
            numbers.Add('X', 10);
            numbers.Add('V', 5);
            numbers.Add('I', 1);
            numbers.Add('m', 1000);
            numbers.Add('d', 500);
            numbers.Add('c', 100);
            numbers.Add('l', 50);
            numbers.Add('x', 10);
            numbers.Add('v', 5);
            numbers.Add('i', 1);
        }

        public string ConvertToArabic()
        {
            int arabicNumber = 0;

            for (int i = 0; i < RomanValue.Count(); i++)
            {
                if (!numbers.ContainsKey(RomanValue[i]))
                    break;
                if (i == RomanValue.Count() - 1)
                    arabicNumber += numbers[RomanValue[i]];
                else
                {
                    if (numbers[RomanValue[i + 1]] > numbers[RomanValue[i]])
                        arabicNumber -= numbers[RomanValue[i]];
                    else
                        arabicNumber += numbers[RomanValue[i]];
                }
            }
            return arabicNumber.ToString();
        }

    }
}

