﻿using RomanConverter.View;
using RomanConverter.ViewModel;
using System.Windows;

namespace RomanConverter
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            new MainWindow()
            {
                DataContext = new MainWindowViewModel()
            }.Show();
        }
    }
}

